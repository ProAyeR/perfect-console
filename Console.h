/*
 * === Code By ProAyeR ===
 * Origin name - 原始文件名: Console.hpp
 * The Perfect Console Class - 几乎完美的控制台类
 * Update time - 更新时间: 2022/7/13 18:18
 * File encode - 文件编码: UTF-8
 * File version - 文件版本: v1.1
 */
#ifdef _WIN32
//操作系统必须是Windows操作系统才能使用
#ifndef _CONSOLE_HPP_
#define _CONSOLE_HPP_

#include <windows.h>
#include <string> //for std::string

using std::string;

namespace perfect_console
{
    // typedef unsigned short wchar; // 弃用的代码, 现在暂时还不想支持unicode
    typedef unsigned short Color; //颜色属性值
    /* clang-format off */
    // 文本属性色彩定义,不推荐直接使用这些文本属性

    const Color FORE_BLACK   = 0;
    const Color FORE_BLUE    = FOREGROUND_BLUE;
    const Color FORE_GREEN   = FOREGROUND_GREEN;
    const Color FORE_CYAN    = FOREGROUND_BLUE | FOREGROUND_GREEN;
    const Color FORE_RED     = FOREGROUND_RED;
    const Color FORE_PURPLE  = FOREGROUND_BLUE | FOREGROUND_RED;
    const Color FORE_YELLOW  = FOREGROUND_RED  | FOREGROUND_GREEN;
    const Color FORE_WHITE   = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
    const Color FORE_GRAY    = FOREGROUND_INTENSITY;
    const Color FORE_BrBLUE  = FOREGROUND_INTENSITY | FOREGROUND_BLUE;
    const Color FORE_BrGREEN = FOREGROUND_INTENSITY | FOREGROUND_GREEN ;
    const Color FORE_BrCYAN  = FOREGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_GREEN;
    const Color FORE_BrRED   = FOREGROUND_INTENSITY | FOREGROUND_RED;
    const Color FORE_BrPURPLE= FOREGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_RED;
    const Color FORE_BrYELLOW= FOREGROUND_INTENSITY | FOREGROUND_RED  | FOREGROUND_GREEN;
    const Color FORE_BrWHITE = FOREGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;

    const Color BACK_BLACK   = 0;
    const Color BACK_BLUE    = BACKGROUND_BLUE;
    const Color BACK_GREEN   = BACKGROUND_GREEN;
    const Color BACK_CYAN    = BACKGROUND_BLUE | BACKGROUND_GREEN;
    const Color BACK_RED     = BACKGROUND_RED;
    const Color BACK_PURPLE  = BACKGROUND_BLUE | BACKGROUND_RED;
    const Color BACK_YELLOW  = BACKGROUND_RED  | BACKGROUND_GREEN;
    const Color BACK_WHITE   = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED;
    const Color BACK_GRAY    = BACKGROUND_INTENSITY;
    const Color BACK_BrBLUE  = BACKGROUND_INTENSITY | BACKGROUND_BLUE;
    const Color BACK_BrGREEN = BACKGROUND_INTENSITY | BACKGROUND_GREEN;
    const Color BACK_BrCYAN  = BACKGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_GREEN;
    const Color BACK_BrRED   = BACKGROUND_INTENSITY | BACKGROUND_RED;
    const Color BACK_BrPURPLE= BACKGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_RED;
    const Color BACK_BrYELLOW= BACKGROUND_INTENSITY | BACKGROUND_RED  | BACKGROUND_GREEN;
    const Color BACK_BrWHITE = BACKGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED;

    const Color DEFAULT_COLOR= FORE_WHITE | BACK_BLACK;
    // 颜色代码和Cmd内颜色对应的颜色代码类似, 只不过a~f变成了10~15
    typedef unsigned short ColorCode; // 0~15 or 0x0 ~ 0xf
    typedef const char *ColorWord;    // "00" "01" ... "fe" "ff"

    namespace color {
        // 这里的颜色名字使用直观颜色命名, 所以会与程序源码中的颜色不同
        const ColorCode black    = 0;
        const ColorCode navy     = 1;
        const ColorCode olive    = 2;
        const ColorCode cyan     = 3;
        const ColorCode crimson  = 4;
        const ColorCode purple   = 5;
        const ColorCode brown    = 6;
        const ColorCode white    = 7;
        const ColorCode gray     = 8;
        const ColorCode blue     = 9;
        const ColorCode green    = 10;
        const ColorCode azure    = 11;
        const ColorCode red      = 12;
        const ColorCode pink     = 13;
        const ColorCode yellow   = 14;
        const ColorCode pale     = 15;
    }
    /**
     * @brief (内部使用)将颜色代码(ColorCode)转换为颜色属性值(Color)
     *
     * @param Back 背景颜色代码
     * @param Fore 前景(文字)颜色代码
     * @return Color 颜色属性值
     */
    Color GetConsoleColor(ColorCode Back, ColorCode Fore)
    {
        ColorCode B_col, F_col;
        switch(Back)
        {
            case 0 :B_col=BACK_BLACK   ;break;
            case 1 :B_col=BACK_BLUE    ;break;
            case 2 :B_col=BACK_GREEN   ;break;
            case 3 :B_col=BACK_CYAN    ;break;
            case 4 :B_col=BACK_RED     ;break;
            case 5 :B_col=BACK_PURPLE  ;break;
            case 6 :B_col=BACK_YELLOW  ;break;
            case 7 :B_col=BACK_WHITE   ;break;
            case 8 :B_col=BACK_GRAY    ;break;
            case 9 :B_col=BACK_BrBLUE  ;break;
            case 10:B_col=BACK_BrGREEN ;break;
            case 11:B_col=BACK_BrCYAN  ;break;
            case 12:B_col=BACK_BrRED   ;break;
            case 13:B_col=BACK_BrPURPLE;break;
            case 14:B_col=BACK_BrYELLOW;break;
            case 15:B_col=BACK_BrWHITE ;break;
            default:B_col=BACK_BLACK   ;break;//无法识别默认输出黑色 
        }
        switch(Fore)
        {
            case 0 :F_col=FORE_BLACK   ;break;
            case 1 :F_col=FORE_BLUE    ;break;
            case 2 :F_col=FORE_GREEN   ;break;
            case 3 :F_col=FORE_CYAN    ;break;
            case 4 :F_col=FORE_RED     ;break;
            case 5 :F_col=FORE_PURPLE  ;break;
            case 6 :F_col=FORE_YELLOW  ;break;
            case 7 :F_col=FORE_WHITE   ;break;
            case 8 :F_col=FORE_GRAY    ;break;
            case 9 :F_col=FORE_BrBLUE  ;break;
            case 10:F_col=FORE_BrGREEN ;break;
            case 11:F_col=FORE_BrCYAN  ;break;
            case 12:F_col=FORE_BrRED   ;break;
            case 13:F_col=FORE_BrPURPLE;break;
            case 14:F_col=FORE_BrYELLOW;break;
            case 15:F_col=FORE_BrWHITE ;break;
            default:F_col=FORE_WHITE   ;break;//无法识别默认输出白色 
        }
        return (B_col | F_col);
    }

    /**
     * @brief (内部使用)将颜色设置文本(ColorWord)转换为颜色属性值(Color)
     * 
     * @param ptr 颜色设置文本, 与cmd中color指令相同, 使用一个字符串设置颜色, 
     *            颜色字符'0'~'9','a'~'f'代表16种颜色, 
     *            第一个字符为背景色, 
     *            第二个字符为前景(文本)色, 
     *            传递参数如"9f","07"等字符串即可
     * @return Color 颜色属性值
     */
    Color GetConsoleColor(ColorWord ptr)
    {
        ColorCode B_col, F_col;
        switch(ptr[0])
        {
            case '\0':return DEFAULT_COLOR; //以防空字符串
            case '0':B_col=BACK_BLACK   ;break;
            case '1':B_col=BACK_BLUE    ;break;
            case '2':B_col=BACK_GREEN   ;break;
            case '3':B_col=BACK_CYAN    ;break;
            case '4':B_col=BACK_RED     ;break;
            case '5':B_col=BACK_PURPLE  ;break;
            case '6':B_col=BACK_YELLOW  ;break;
            case '7':B_col=BACK_WHITE   ;break;
            case '8':B_col=BACK_GRAY    ;break;
            case '9':B_col=BACK_BrBLUE  ;break;
            case 'a':B_col=BACK_BrGREEN ;break;
            case 'b':B_col=BACK_BrCYAN  ;break;
            case 'c':B_col=BACK_BrRED   ;break;
            case 'd':B_col=BACK_BrPURPLE;break;
            case 'e':B_col=BACK_BrYELLOW;break;
            case 'f':B_col=BACK_BrWHITE ;break;
            default :B_col=BACK_BLACK   ;break;//无法识别默认输出黑色 
        }
        switch(ptr[1])
        {
            case '0':F_col=FORE_BLACK   ;break;
            case '1':F_col=FORE_BLUE    ;break;
            case '2':F_col=FORE_GREEN   ;break;
            case '3':F_col=FORE_CYAN    ;break;
            case '4':F_col=FORE_RED     ;break;
            case '5':F_col=FORE_PURPLE  ;break;
            case '6':F_col=FORE_YELLOW  ;break;
            case '7':F_col=FORE_WHITE   ;break;
            case '8':F_col=FORE_GRAY    ;break;
            case '9':F_col=FORE_BrBLUE  ;break;
            case 'a':F_col=FORE_BrGREEN ;break;
            case 'b':F_col=FORE_BrCYAN  ;break;
            case 'c':F_col=FORE_BrRED   ;break;
            case 'd':F_col=FORE_BrPURPLE;break;
            case 'e':F_col=FORE_BrYELLOW;break;
            case 'f':F_col=FORE_BrWHITE ;break;
            default :F_col=FORE_WHITE   ;break;//无法识别默认输出白色 
        }
        return (B_col | F_col);
    }
    /* clang-format on */

    //常用结构体重命名
    typedef COORD Position;     // short X,Y
    typedef SMALL_RECT Section; // short Left,Top,Right,Bottom

//来自函数GetEventCode的鼠标事件常数
//这些常数在wincon.h里面的定义不如这样来看得懂, 一般的电脑使用者不会有超过3个按键的鼠标
#define MOUSE_LEFT_BUTTON_PRESSED 0x1
#define MOUSE_RIGHT_BUTTON_PRESSED 0x2
#define MOUSE_MIDDLE_BUTTON_PRESSED 0x4
#define MOUSE_DOUBLE_CLICK -1
#define MOUSE_ROLL_UP 0x8
#define MOUSE_ROLL_DOWN -0x8

//来自函数SetCodePage的代码页常数
//这里的代码页理论上来说每台设备的不同, 但是考虑到一般用户不会自行修改代码页, 所以这里使用了最常用的三个默认值
#define CODEPAGE_ANSI 437
#define CODEPAGE_GB2312 936
#define CODEPAGE_UTF_8 65001
// 更多代码页可以查看https://blog.csdn.net/sxzlc/article/details/106270405
/*
    MS-DOS支持的代码页只有
    437 美国
    850 多语言（拉丁文 I）
    852 斯拉夫语（拉丁文 II）
    855 西里尔文（俄语）
    857 土耳其语
    860 葡萄牙语
    861 冰岛语
    863 加拿大 - 法语
    865 日耳曼语
    866 俄语
    869 现代希腊语
    936 简体中文（是GBK、GB2312）
    950 繁体中文（是大五码）
    65001 UTF-8（万国语）
    需要使用的可以直接使用常数, 比如SetCodePage(866)就是俄语支持
    如果是无效代码页, 设置会失败
*/

//来自函数DisableButton的按钮常数
//按钮的常数定义, 这里不能保证是否与已有常数冲突
#define CLOSE_BUTTON 0x1
#define MAX_BUTTON 0x2
#define MIN_BUTTON 0x4

    /**
     * @brief 光标类, 包含各种控制台光标控制函数
     */
    class Cursor
    {
    private:
        HANDLE hConsole;

    public:
        Cursor() { hConsole = GetStdHandle(STD_OUTPUT_HANDLE); }
        Cursor(HANDLE h) : hConsole(h) {}

        virtual void SetPos(short x, short y);
        virtual void SetPos(Position pos);
        virtual Position GetPos();
        virtual bool SetSize(short size);
        virtual short GetSize();
        virtual bool SetVisible(bool v);
        virtual bool GetVisible();
    };
    typedef Cursor Cur; //简写方便使用, 如console.Cur::SetPos(0,0);

    /**
     * @brief 控制台事件类, 目前只支持输入事件检测
     */
    class Event
    {
    private:
        HANDLE hConsole;
        HANDLE hStdInput;

    public:
        Event() : hConsole(GetStdHandle(STD_OUTPUT_HANDLE)), hStdInput(GetStdHandle(STD_INPUT_HANDLE)) {}
        Event(HANDLE h) : hConsole(h), hStdInput(GetStdHandle(STD_INPUT_HANDLE)) {}

        virtual int GetEventCode(bool IsSetCurPos);
        virtual int GetKeyEvent(int *out_ControlKeyState);
        virtual INPUT_RECORD GetEvent();
    };
    typedef Event Evt; //简写方便使用

    /**
     * @brief 几乎完美的控制台类, 继承自光标类与事件检测类
     */
    class Console : public Cur, public Evt
    {
    public:
        HANDLE hConsole; //这两个句柄可以公开来供用户使用
        HWND hwndConsole;
        Console() : hConsole(GetStdHandle(STD_OUTPUT_HANDLE)), hwndConsole(GetForegroundWindow()) {} //两个继承类的handle会自动设置
        ~Console()
        {
            // 控制台类析构时会尝试将颜色属性设置为默认值
            SetColor(); // 如果你不需要这样的功能, 请自行删除该段代码, 并在发行时附带修改说明
            CloseHandle(hConsole);
        }
        //默认参数只写在声明处, 定义处不需要默认参数
        virtual bool SetOutputCodePage(unsigned int codepage);
        virtual bool SetInputCodePage(unsigned int codepage);
        virtual bool SetCodePage(unsigned int codepage);
        virtual const char *SetColor(Color clr = DEFAULT_COLOR);
        virtual const char *SetColor(ColorCode Back_Color, ColorCode Fore_Color);
        virtual const char *SetColor(ColorWord ColorWord);
        virtual unsigned long PutText(string str, Position target);
        virtual unsigned long PutText(string str, short x, short y);
        virtual bool MoveText(Section sec, Position target);
        virtual bool MoveText(short rect_Left, short rect_Top, short rect_Right, short rect_Bottom, short x, short y);
        virtual unsigned long FillColor(Section sec, Color clr);
        virtual unsigned long FillColor(short rect_Left, short rect_Top, short rect_Right, short rect_Bottom, Color clr);
        virtual void Pause(string text = "");
        virtual void Delay(unsigned long ms);
        virtual void Clean(Section sec);
        virtual void CleanScreen();
        virtual bool SetScreenSize(short w, short h);
        virtual Position GetScreenSize();
        virtual Section GetScreenSec();
        virtual bool SetWindowSize(short w, short h);
        virtual void SetSize(short w, short h);
        virtual void FullScreen();
        virtual void MaximizeSize();
        virtual void Maximize();
        virtual void Minimize();
        virtual void Hide();
        virtual void Show();
        virtual void Move(int x, int y);
        virtual Position Where();
        virtual void SetTitle(string str);
        virtual string GetTitle();
        virtual void DisableButton(int button_index);
        virtual void DisableCtrlC();
        virtual void SetBitmapFont(Position font_size = {8, 16}, int font_weight = FW_NORMAL);
        virtual void SetFont(string font_name, int font_size = 18, int font_weight = FW_NORMAL);
        virtual void SetFont(const wchar_t *font_name, int font_size = 18, int font_weight = FW_NORMAL);
        virtual void SetFontSize(int font_size);
        virtual void SetFontWeigh(int font_weight);
        virtual Position GetFontSize();
        virtual int GetFontWeigh();
        virtual string GetFontName();
    };
}

#include "Console.cpp"
#endif
#else
#error "Console.hpp can't be use in None-windows OS!"
#endif