/*
 * === Code By ProAyeR ===
 * Origin name - 原始文件名: Console.cpp
 * The Perfect Console Class - 几乎完美的控制台类
 * Update time - 更新时间: 2022/7/13 18:18
 * File encode - 文件编码: UTF-8
 * File version - 文件版本: v1.1
 */
#ifdef _WIN32
//操作系统必须是Windows操作系统才能使用
#ifndef _CONSOLE_CPP_
#define _CONSOLE_CPP_

#include <windows.h>
#include <iostream> //at console.Pause() for std::cout
#include <conio.h>  //at console.Pause() for getch()
#include <string>   //for std::string
#include "Console.h"

using std::cout;
using std::string;

//常用宏的定义
#define LIMIT_IN(a, x, y) (a >= y) ? y : ((a <= x) ? x : a)

namespace perfect_console
{
    //////////////////////////////////////////////////////////////////////////////////Cursor
    /**
     * @brief 设置光标位置, 左上角为0,0
     *
     * @param x 光标x位置
     * @param y 光标y位置
     */
    inline void Cursor::SetPos(short x, short y) { SetConsoleCursorPosition(hConsole, {x, y}); }

    /**
     * @brief 设置光标位置, 左上角为{0,0}
     *
     * @param pos 光标位置结构
     */
    inline void Cursor::SetPos(Position pos) { SetConsoleCursorPosition(hConsole, pos); }

    /**
     * @brief 获取光标位置
     *
     * @return Position 光标的当前位置
     */
    inline Position Cursor::GetPos()
    {
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(hConsole, &csbi);
        return csbi.dwCursorPosition;
    }

    /**
     * @brief 设置光标的大小
     *
     * @param size 支持1~100的数, 1为一条直线, 100为一整个方块
     * @return bool 是否设置成功
     */
    inline bool Cursor::SetSize(short size)
    {
        CONSOLE_CURSOR_INFO cci;
        size = LIMIT_IN(size, 1, 100);
        GetConsoleCursorInfo(hConsole, &cci);
        cci.dwSize = size;
        return SetConsoleCursorInfo(hConsole, &cci);
    }

    /**
     * @brief 获得光标的大小
     *
     * @return short 光标当前大小, 1~100
     */
    inline short Cursor::GetSize()
    {
        CONSOLE_CURSOR_INFO cci;
        GetConsoleCursorInfo(hConsole, &cci);
        return (short)cci.dwSize;
    }

    /**
     * @brief 设置光标是否可见
     *
     * @param v 光标是否可见, true为可见, false为不可见
     * @return bool 是否设置成功
     */
    inline bool Cursor::SetVisible(bool v)
    {
        CONSOLE_CURSOR_INFO cci;
        GetConsoleCursorInfo(hConsole, &cci);
        cci.bVisible = v;
        return SetConsoleCursorInfo(hConsole, &cci);
    }

    /**
     * @brief 获得光标可见状态
     *
     * @return bool 为 true则当前光标可见, false则当前光标不可见
     */
    inline bool Cursor::GetVisible()
    {
        CONSOLE_CURSOR_INFO cci;
        GetConsoleCursorInfo(hConsole, &cci);
        return cci.bVisible; //吐槽:看不看得见用眼睛就可以看, 好像不需要用程序实现
    }

    //////////////////////////////////////////////////////////////////////////////////Event
    /**
     * @brief 获得控制台输入事件代码
     *
     * @param IsSetCurPos 是否设置鼠标位置, 如果为真, 则在执行函数过程中会将光标位置为设置到鼠标位置处
     * @return int 事件代码具体规则如下, 
     * 如果是鼠标事件：
     * 返回1为左键按下, 2为右键按下, 4为中键按下, 8为滚轮向上滑动, 
     * 返回-1为双击左键, -2为双击右键, -4为双击中键,  -8为滚轮向下滑动, 
     * 若左键右键同时按下返回3, 负数同理, 
     * 本函数并不检测又双击又滑轮的花哨操作, 也不支持按钮超过3个的鼠标；
     *
     * 如果是键盘事件：
     * 返回键盘键值, 各个键的键值可自行写代码实验获得, 
     * 不支持组合键检测, 类似于ctrl+c的自闭组合就无法检测, 
     * 不过代码编写者可以使用连续嵌套检测的方法实现
     */
    int Event::GetEventCode(bool IsSetCurPos) //参数为是否修改鼠标位置
    {
        CONSOLE_SCREEN_BUFFER_INFO csbi;            //定义窗口缓冲区信息结构体
        INPUT_RECORD rec;                           //定义输入事件结构体
        unsigned long res;                          //用于存储读取记录
        ReadConsoleInput(hStdInput, &rec, 1, &res); //读取输入事件
        if (IsSetCurPos)                            //由用户指定是否修改鼠标位置, 以防出现某些错误
            SetConsoleCursorPosition(hConsole, rec.Event.MouseEvent.dwMousePosition);
        if (rec.EventType == MOUSE_EVENT) //如果当前为鼠标事件
        {
            if (rec.Event.MouseEvent.dwEventFlags == MOUSE_WHEELED)             //先检测滑轮
                return ((long)rec.Event.MouseEvent.dwButtonState < 0) ? -8 : 8; //原本是无符号的,这里要进行转换
            if (rec.Event.MouseEvent.dwEventFlags == DOUBLE_CLICK)
                return -rec.Event.MouseEvent.dwButtonState;
            else
                return rec.Event.MouseEvent.dwButtonState; //不符合以上情况直接返回原值
        }
        if (rec.EventType == KEY_EVENT) //如果当前为键盘事件
            if (rec.Event.KeyEvent.bKeyDown == TRUE)
                return rec.Event.KeyEvent.wVirtualKeyCode;
        return 0;
    }

    /**
     * @brief 获得控制台键盘输入事件, 当键被按下时返回
     *
     * @param out_ControlKeyState 接受int类型指针, 用于返回控制键状态
     * @return int 返回键盘键值
     */
    int Event::GetKeyEvent(int *out_ControlKeyState) //得到完整的事件, 还需要一个返回值
    {
        INPUT_RECORD rec;
        DWORD res;
        ReadConsoleInput(hStdInput, &rec, 1, &res);
        if (rec.EventType == KEY_EVENT && rec.Event.KeyEvent.bKeyDown)
        {
            *out_ControlKeyState = rec.Event.KeyEvent.dwControlKeyState;
            return rec.Event.KeyEvent.wVirtualKeyCode;
        }
        return 0;
    }

    /**
     * @brief 获得控制台输入事件, 给有高级输入检测需要的用户使用
     *
     * @return INPUT_RECORD 返回完整INPUT_RECORD结构体
     */
    inline INPUT_RECORD Event::GetEvent()
    {
        INPUT_RECORD rec; //请自行查看INPUT_RECORD的定义以获得全部的功能支持
        DWORD res;
        ReadConsoleInput(hStdInput, &rec, 1, &res);
        return rec;
    }

    //////////////////////////////////////////////////////////////////////////////////Console
    /**
     * @brief 设置控制台输出代码页
     *
     * @param codepage 代码页常数
     * @return 是否设置成功
     */
    inline bool Console::SetOutputCodePage(unsigned int codepage) { return SetConsoleOutputCP(codepage); }

    /**
     * @brief 设置控制台输入代码页
     *
     * @param codepage 代码页常数
     * @return 是否设置成功
     */
    inline bool Console::SetInputCodePage(unsigned int codepage) { return SetConsoleCP(codepage); }

    /**
     * @brief 设置控制台代码页, 包括输入代码页和输出代码页
     *
     * @param codepage 代码页常数
     * @return 是否设置成功
     */
    inline bool Console::SetCodePage(unsigned int codepage) { return SetOutputCodePage(codepage) && SetInputCodePage(codepage); }

    /**
     * @brief 设置输出颜色属性
     *
     * @param clr 颜色属性
     * @return const char* 返回空文本以便使用在输出流中
     */
    const char *Console::SetColor(Color clr)
    {
        SetConsoleTextAttribute(hConsole, clr);
        return ""; /* 返回空字符使得以下的代码可以实现
        cout << console.SetColor(...) << "text"; */
    }

    /**
     * @brief 设置输出颜色属性
     *
     * @param Back_Color 背景颜色代码,0~15或0x0~0xf或color::下颜色常量
     * @param Fore_Color 前景(文本)颜色代码
     * @return const char* 返回空文本以便使用在输出流中
     */
    inline const char *Console::SetColor(ColorCode Back_Color, ColorCode Fore_Color) { return SetColor(GetConsoleColor(Back_Color, Fore_Color)); }

    /**
     * @brief 设置输出颜色属性
     *
     * @param ColorWord 颜色设置文本, 与cmd中color指令相同, 使用一个字符串设置颜色, 
     *                  颜色字符'0'~'9','a'~'f'代表16种颜色, 
     *                  第一个字符为背景色, 
     *                  第二个字符为前景(文本)色, 
     *                  传递参数如"9f","07"等字符串即可
     * @return const char* 返回空文本以便使用在输出流中
     */
    inline const char *Console::SetColor(ColorWord ColorWord) { return SetColor(GetConsoleColor(ColorWord)); }

    /**
     * @brief 修改某处文本, 不改变颜色属性
     *
     * @param str 用于替换的文本
     * @param target 替换文本的位置
     * @return unsigned long 替换的字符个数
     */
    inline unsigned long Console::PutText(string str, Position target)
    {
        unsigned long lpNumberOfCharsWritten;
        WriteConsoleOutputCharacterA(hConsole, str.c_str(), str.length(), target, &lpNumberOfCharsWritten);
        return lpNumberOfCharsWritten;
    }

    /**
     * @brief 修改某处文本, 不改变颜色属性
     *
     * @param str 用于替换的文本
     * @param x 替换文本的X位置
     * @param y 替换文本的Y位置
     * @return unsigned long 替换的字符个数
     */
    inline unsigned long Console::PutText(string str, short x, short y) { return PutText(str, {x, y}); }

    /**
     * @brief 移动某处文本, 包括颜色属性等
     *
     * @param sec 需要移动的区域
     * @param target 区域移动到的地点, 此参数为目标区域的左上角位置
     * @return 是否移动成功
     */
    bool Console::MoveText(Section sec, Position target)
    {
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(hConsole, &csbi);
        CHAR_INFO chFill;
        chFill.Char.AsciiChar = ' '; //用于填充的字符
        chFill.Attributes = csbi.wAttributes;
        return ScrollConsoleScreenBufferA(hConsole, &sec, NULL, target, &chFill);
    }

    /**
     * @brief 移动某处文本, 包括颜色属性等
     *
     * @param rect_Left 需要移动的区域设置, 左坐标
     * @param rect_Top 顶坐标
     * @param rect_Right 右坐标
     * @param rect_Bottom 底坐标
     * @param x 区域移动到的地点, 此参数为目标区域的左上角X位置
     * @param y Y位置
     * @return 是否移动成功
     */
    inline bool Console::MoveText(short rect_Left, short rect_Top, short rect_Right, short rect_Bottom,
                                  short x, short y) { return MoveText({rect_Left, rect_Top, rect_Right, rect_Bottom}, {x, y}); }

    /**
     * @brief 改变一个区域的颜色属性
     *
     * @param sec 目标区域
     * @param clr 颜色属性, 使用GetConsoleColor函数获得 TODO:将颜色作为一个类呈现
     * @return unsigned long 填充了多少字体的颜色属性
     */
    unsigned long Console::FillColor(Section sec, Color clr)
    {
        Position posRect = {sec.Left, sec.Top};
        unsigned long lpNumberOfAttrsWritten = 0;
        for (int i = 0; i <= sec.Bottom - sec.Top; i++)
        {
            unsigned long lpNumber;
            FillConsoleOutputAttribute(hConsole, clr, sec.Right - sec.Left + 1, posRect, &lpNumber);
            lpNumberOfAttrsWritten += lpNumber;
            posRect.Y++;
        }
        return lpNumberOfAttrsWritten;
    }

    /**
     * @brief 改变一个区域的颜色属性
     *
     * @param rect_Left 目标区域, 左坐标
     * @param rect_Top 顶坐标
     * @param rect_Right 右坐标
     * @param rect_Bottom 底坐标
     * @param clr 颜色属性, 使用GetConsoleColor函数获得
     * @return unsigned long 填充了多少字体的颜色属性
     */
    inline unsigned long Console::FillColor(short rect_Left, short rect_Top, short rect_Right, short rect_Bottom,
                                            Color clr) { return FillColor({rect_Left, rect_Top, rect_Right, rect_Bottom}, clr); }

    /**
     * @brief 控制台暂停, 使用getch()实现
     *
     * @param text 暂停时需要输出的文本, 输出到cout流
     */
    inline void Console::Pause(string text)
    {
        cout << text;
        getch();
    }

    /**
     * @brief 延迟函数, 使用Sleep()实现
     *
     * @param ms 延迟的毫秒数
     */
    inline void Console::Delay(unsigned long ms) { Sleep(ms); }

    /**
     * @brief 清空选定内容, 颜色属性将使用当前值
     *
     * @param sec 需要清空的区域
     */
    void Console::Clean(Section sec)
    {
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(hConsole, &csbi);
        Position pos = {sec.Left, sec.Top};
        unsigned long cCharsWritten;
        unsigned long dwConSize = sec.Right - sec.Left; //当前缓冲区可容纳的字符数
        for (int i = sec.Top; i <= sec.Bottom; i++)
        {
            pos.Y = i;
            FillConsoleOutputCharacterA(hConsole, ' ', dwConSize, pos, &cCharsWritten);             //用空格填充缓冲区
            FillConsoleOutputAttribute(hConsole, csbi.wAttributes, dwConSize, pos, &cCharsWritten); //填充缓冲区属性
        }
        pos = {0, 0};
        SetConsoleCursorPosition(hConsole, pos);
    }

    /**
     * @brief 清空屏幕内容, 颜色属性将使用当前值
     *
     */
    inline void Console::CleanScreen() { Clean(GetScreenSec()); }

    /**
     * @brief 设置屏幕缓冲区大小, 即可写字符区域的大小
     *
     * @param w 宽, 单位为字符
     * @param h 高, 单位为字符
     * @return 是否成功
     */
    inline bool Console::SetScreenSize(short w, short h) { return SetConsoleScreenBufferSize(hConsole, {w, h}); }

    /**
     * @brief 获取屏幕缓冲区大小, 即可写字符区域的大小
     *
     * @return Position XY分别代表区域大小的宽和高
     */
    inline Position Console::GetScreenSize()
    {
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(hConsole, &csbi);
        return csbi.dwSize;
    }

    /**
     * @brief 获取屏幕缓冲区大小, 即可写字符区域的大小
     *
     * @return Section 区域的选区, 即{0, 0, 宽, 高}
     */
    inline Section Console::GetScreenSec()
    {
        Section sec;
        sec.Left = sec.Top = 0;
        Position size = GetScreenSize();
        sec.Right = size.X;
        sec.Bottom = size.Y;
        return sec;
    }

    /**
     * @brief 设置窗口大小, 即可见区域大小
     *
     * @param w 宽, 单位为字符
     * @param h 高, 单位为字符
     * @return 是否设置成功
     */
    inline bool Console::SetWindowSize(short w, short h)
    {
        SMALL_RECT rect = {0, 0, --w, --h};
        return SetConsoleWindowInfo(hConsole, 1, &rect);
    }

    /**
     * @brief 直接设置控制台窗口大小
     *
     * @param w 宽, 单位为字符
     * @param h 高, 单位为字符
     */
    inline void Console::SetSize(short w, short h)
    {
        //窗口大小的改变会导致整个控制台窗口的重绘
        //这可能会导致颜色设置的混乱, 建议在改变颜色之前就确定好窗口的大小
        SetWindowSize(0, 0);
        SetWindowSize(w, h);
        SetScreenSize(w, h);
        SetWindowSize(w, h);
    }

    /**
     * @brief 设置控制台窗口为全屏(窗口全屏)
     *
     */
    inline void Console::FullScreen()
    {
        Move(-8, 0); //不知道为什么, 我的电脑上需要这样一个常数进行修正, 才看起来像全屏
        //或许在你自己使用的过程中需要自己调整
        Position pos = GetLargestConsoleWindowSize(hConsole);
        SetSize(pos.X, pos.Y);
    }

    /**
     * @brief 设置控制台窗口的尺寸为最大, 仅仅改变尺寸
     *
     */
    inline void Console::MaximizeSize()
    {
        int cx = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度 像素
        int cy = GetSystemMetrics(SM_CYSCREEN); //屏幕高度 像素
        SetWindowPos(hwndConsole, HWND_TOP, 0, 0, cx, cy, 0);
    }

    /**
     * @brief 最大化控制台窗口
     *
     */
    inline void Console::Maximize() { ShowWindow(hwndConsole, SW_MAXIMIZE); }

    /**
     * @brief 最小化控制台窗口
     *
     */
    inline void Console::Minimize() { ShowWindow(hwndConsole, SW_MINIMIZE); }

    /**
     * @brief 隐藏控制台窗口
     *
     */
    inline void Console::Hide() { ShowWindow(hwndConsole, SW_HIDE); }

    /**
     * @brief 显示控制台窗口, 用于显示隐藏的控制台窗口
     *
     */
    inline void Console::Show() { ShowWindow(hwndConsole, SW_NORMAL); }

    /**
     * @brief 移动控制台窗口相对桌面的位置
     *
     * @param x 横坐标, 单位为像素
     * @param y 纵坐标, 单位为像素
     */
    inline void Console::Move(int x, int y)
    {
        RECT rect;
        GetWindowRect(hwndConsole, &rect);
        int w = rect.right - rect.left;
        int h = rect.bottom - rect.top;
        rect.left = x;
        rect.top = y;
        MoveWindow(hwndConsole, rect.left, rect.top, w, h, true);
    }

    /**
     * @brief 获取当前控制台窗口相对桌面的位置
     *
     * @return Position XY分别为窗口所在位置的横坐标与纵坐标, 单位为像素
     */
    inline Position Console::Where()
    {
        RECT rect;
        GetWindowRect(hwndConsole, &rect);
        return {(short)rect.left, (short)rect.top};
    }

    /**
     * @brief 设置控制台窗口的标题, 不支持宽字符
     *
     * @param str 需要设置为的标题文本, 请使用非宽字符的编码(如GB2312)
     */
    inline void Console::SetTitle(string str) { SetConsoleTitleA(str.c_str()); }

    /**
     * @brief 获取当前控制台窗口的标题
     *
     * @return string 返回当前控制台窗口的标题
     */
    inline string Console::GetTitle()
    {
        char charbuff[256];
        GetConsoleTitleA(charbuff, 256);
        string str = (string)charbuff;
        return str;
    }

    /**
     * @brief 禁止控制台窗口按钮
     *
     * @param button_index 按钮索引, 
     * 具体支持CLOSE_BUTTON, MIN_BUTTON与MAX_BUTTON, 
     * 即关闭按钮, 最小化按钮与最大化按钮, 
     * 使用逻辑或将其链接可同时设置
     */
    void Console::DisableButton(int button_index)
    {
        if (button_index & CLOSE_BUTTON)
        {
            HMENU hmenu = GetSystemMenu(hwndConsole, false);
            RemoveMenu(hmenu, SC_CLOSE, MF_BYCOMMAND);
        }
        if ((button_index & MIN_BUTTON) || (button_index & MAX_BUTTON)) //为了让三个按钮都能够识别
        {
            HMENU hmenu = GetSystemMenu(hwndConsole, false);
            LONG style = GetWindowLong(hwndConsole, GWL_STYLE);
            if (button_index & MIN_BUTTON)
                style &= ~(WS_MINIMIZEBOX);
            if (button_index & MAX_BUTTON)
                style &= ~(WS_MAXIMIZEBOX);
            SetWindowLong(hwndConsole, GWL_STYLE, style);
            DestroyMenu(hmenu);
        }
    }

    /**
     * @brief 禁止控制台的Ctrl+C关闭事件
     * 使用之后Ctrl+C将不再直接关闭程序, 而是会将其作为正常的键盘输入读入控制台
     *
     */
    inline void Console::DisableCtrlC() { SetConsoleCtrlHandler(NULL, true); }

    /**
     * @brief 设置控制台的点阵字体
     *
     * @param font_size 点阵字体的大小, X为横向大小, Y为纵向大小, 单位均为像素
     * @param font_weight 点阵字体的粗细, 取值从100~1000
     */
    void Console::SetBitmapFont(Position font_size, int font_weight)
    {
        CONSOLE_FONT_INFOEX cfi;
        cfi.cbSize = sizeof(cfi);
        cfi.nFont = 0;
        cfi.dwFontSize = font_size;
        cfi.FontFamily = FF_DONTCARE;
        cfi.FontWeight = LIMIT_IN(font_weight, 100, 1000); // font_weigh取值从100-1000

        wcscpy_s(cfi.FaceName, L"Raster"); //点阵字体
        SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
    }

    /**
     * @brief 设置控制台的字体, 暂不支持中文字体
     *
     * @param font_name 字体名称, 使用英文字体名称, 字体名称以系统为准
     * @param font_size 字体大小, 默认为18
     * @param font_weight 字体粗细, 取值从100~1000
     */
    void Console::SetFont(string font_name, int font_size, int font_weight)
    {
        CONSOLE_FONT_INFOEX cfi;
        cfi.cbSize = sizeof(cfi);
        cfi.nFont = 0;
        cfi.dwFontSize.X = 0;
        cfi.dwFontSize.Y = font_size; //非点阵字体只需要设置Y就可以了
        cfi.FontFamily = FF_DONTCARE;
        cfi.FontWeight = LIMIT_IN(font_weight, 100, 1000); // font_weigh取值从100-1000
        // wcscpy_s(cfi.FaceName, TEXT(font_name.c_str()));
        for (int i = 0; i < font_name.length() && i < 32; i++) // easy strcpy,这里会导致中文字的丢失,英文字体不受影响
            cfi.FaceName[i] = font_name.at(i);
        SetCurrentConsoleFontEx(hConsole, false, &cfi);
        //测试后发现无论是否正确修改都会返回真, 所以索性就不要返回值了
    }

    /**
     * @brief 设置控制台的字体
     *
     * @param font_name 字体名称, 支持中文字体, 输入请使用宽字符, 即 L"黑体" 等
     * @param font_size 字体大小, 默认为18
     * @param font_weight 字体粗细, 取值从100~1000
     */
    void Console::SetFont(const wchar_t *font_name, int font_size, int font_weight) //为了支持中文字体的妥协方法
    {
        CONSOLE_FONT_INFOEX cfi;
        cfi.cbSize = sizeof(cfi);
        cfi.nFont = 0;
        cfi.dwFontSize.X = 0;
        cfi.dwFontSize.Y = font_size; //非点阵字体只需要设置Y就可以了
        cfi.FontFamily = FF_DONTCARE;
        cfi.FontWeight = LIMIT_IN(font_weight, 100, 1000); // font_weigh取值从100-1000
        wcscpy_s(cfi.FaceName, font_name);                 //赋值字体名
        SetCurrentConsoleFontEx(hConsole, false, &cfi);
    }

    /**
     * @brief 设置控制台字体大小
     *
     * @param font_size 字体大小
     */
    void Console::SetFontSize(int font_size)
    {
        CONSOLE_FONT_INFOEX cfi;
        GetCurrentConsoleFontEx(hConsole, false, &cfi);
        cfi.cbSize = sizeof(cfi);
        cfi.dwFontSize.Y = font_size;
        SetCurrentConsoleFontEx(hConsole, false, &cfi);
    }

    /**
     * @brief 设置控制台字体的粗细
     *
     * @param font_weight 字体粗细值, 取值从100~1000
     */
    void Console::SetFontWeigh(int font_weight)
    {
        CONSOLE_FONT_INFOEX cfi;
        GetCurrentConsoleFontEx(hConsole, false, &cfi);
        cfi.cbSize = sizeof(cfi);
        cfi.FontWeight = LIMIT_IN(font_weight, 100, 1000); // font_weigh取值从100-1000
        SetCurrentConsoleFontEx(hConsole, false, &cfi);
    }

    /**
     * @brief 获得当前控制台字体大小
     *
     * @return Position 字体大小, 非点阵字体只有Y值代表大小, X为0；点阵字体X为宽, Y为高
     */
    inline Position Console::GetFontSize()
    {
        CONSOLE_FONT_INFO cfi;
        GetCurrentConsoleFont(hConsole, false, &cfi);
        return cfi.dwFontSize; //当未使用点阵字体时, 这里的有效成员只有Y
    }

    /**
     * @brief 获得当前控制台字体粗细
     *
     * @return int 字体粗细, 取值为100~1000
     */
    inline int Console::GetFontWeigh()
    {
        CONSOLE_FONT_INFOEX cfi;
        GetCurrentConsoleFontEx(hConsole, false, &cfi);
        return cfi.FontWeight;
    }

    /**
     * @brief 获取字体名, 还未实现
     *
     * @return string 还未实现, 只会返回"NULL"
     */
    string Console::GetFontName()
    {
        /* //这里涉及到wchar_t与char的转换问题, 暂时无法获得
         CONSOLE_FONT_INFOEX cfi;
         GetCurrentConsoleFontEx(hConsole, false, &cfi);
         return (string)cfi.FaceName;
        */
        return "NULL";
    }
}
#endif
#else
#error "Console.cpp can't be use in None-windows OS!"
#endif