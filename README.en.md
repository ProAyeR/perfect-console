# PerfectConsole

####
An almost perfect console class. But consoles are almost exclusively supported on Windows.

#### Software Architecture

In order to reflect the encapsulation of the class, the software purposely puts the definitions of various functions as the methods of the class in the Console class Console. At the same time, in the class definition header file, it also defines many constants that are convenient for users to use.

#### Installation Tutorial

1. Download `Console.cpp` and `Console.hpp` from the project to the local PC
2. Include `Console.hpp` in your project
3. Use the namespace `perfect_console`
4. Instantiate a console class to use console methods, as shown in the following code
```C++
#include "Console.hpp"
#include <iostream>

using namespace std;
using namespace perfect_console;

int main()
{
    Console console;
    console.SetTitle("This is Console title");
    cout << console.SetColor("4e") << "Hello Colorful World!" << endl;
    return 0;
}
```

#### Show you what I got
Quick reference functions
```C++
//class Cursor
void SetPos(short x, short y);
void SetPos(Position pos);
Position GetPos();
bool SetSize(short size);
short GetSize();
bool SetVisible(bool v);
bool GetVisible();

//class Event
int GetEventCode(bool IsSetCurPos);
int GetKeyEvent(int *out_ControlKeyState);
INPUT_RECORD GetEvent();

//class Console : public Cursor, public Event
bool SetOutputCodePage(unsigned int codepage);
bool SetInputCodePage(unsigned int codepage);
bool SetCodePage(unsigned int codepage);
const char *SetColor(Color clr = DEFAULT_COLOR);
const char *SetColor(ColorCode Back_Color, ColorCode Fore_Color);
const char *SetColor(ColorWord ColorWord);
unsigned long PutText(string str, Position target);
unsigned long PutText(string str, short x, short y);
bool MoveText(Section sec, Position target);
bool MoveText(short rect_Left, short rect_Top, short rect_Right, short rect_Bottom, short x, short y);
unsigned long FillColor(Section sec, Color clr);
unsigned long FillColor(short rect_Left, short rect_Top, short rect_Right, short rect_Bottom, Color clr)
void Pause(string text = "");
void Delay(unsigned long ms);
void Clean(Section sec);
void CleanScreen();
bool SetScreenSize(short w, short h);
Position GetScreenSize();
Section GetScreenSec();
bool SetWindowSize(short w, short h);
void SetSize(short w, short h);
void FullScreen();
void MaximizeSize();
void Maximize();
void Minimize();
void Hide();
void Show();
void Move(int x, int y);
Position Where();
void SetTitle(string str);
string GetTitle();
void DisableButton(int button_index);
void DisableCtrlC();
void SetBitmapFont(Position font_size = {8, 16}, int font_weight = FW_NORMAL);
void SetFont(string font_name, int font_size = 18, int font_weight = FW_NORMAL);
void SetFont(const wchar_t *font_name, int font_size = 18, int font_weight = FW_NORMAL);
void SetFontSize(int font_size);
void SetFontWeigh(int font_weight);
Position GetFontSize();
int GetFontWeigh();
string GetFontName();
```

#### Instructions

For detailed code instructions, see the examples in the examples folder under the project
The sample code uses what the author considers standard writing and usage scenarios, and all code contains comments
This can help you get started with the library quickly

It is also recommended that you use overloaded methods to convert functions in a class to C functions to support C

In addition, in the program source file, I added instructions for each function and class, if you use `Visual Studio Code` for editing
When you hover your mouse over the function name, you will see the description of each function, its arguments, and its return value

#### Precautions

Use existing function and advoid using `system()` , if you still need to use this function, 
please note that this function will disable cursor detection in the console window

#### What's the point?

For me personally, writing console classes solidified my understanding of the various C++ syntax features I learned
At the same time, through the writing of this class I have a preliminary feeling in the use of other people's API should be how to look up information
And I learn how to write my own third-party library

For begining programmers, the library provides a lot of less useful but fancy features to make your interface different
If you study it deeply, you can write procedural interfaces or even graphical interfaces that are very operable and aesthetically pleasing

I would say that it's a little boring for white to just face the black and white console
When it comes to adding color to the console, a few words of code on the web that isn't a special system can make you confused
This library is designed to make your development more colorful and interesting
A platform for junior developers full of ideas and creativity to write their own code within the conformation of the console