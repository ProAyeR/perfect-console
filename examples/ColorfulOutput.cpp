#include <iostream>
#include <iomanip>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

int main()
{
    Console console; // 控制台类 console

    /* 以下为颜色枚举输出 */
    cout << "ColorfulOutput.cpp" << endl;
    for (int i = 0; i <= 15; i++)
    {
        for (int j = 0; j <= 15; j++)
        {
            cout << console.SetColor(i, j) << setw(2) << i << "," << setw(2) << j;
        }
        cout << endl;
    }
    console.Pause();
    console.CleanScreen();
    /* 接下来展示设置颜色的几种办法 */
    // 直接使用定义好的常数,这里的常数命名使用了直观颜色，而不是代码定义中的颜色
    console.SetColor(color::blue, color::white);
    cout << "hi, this is colorful output!" << endl;
    // 使用数字进行定义，这里的数字支持各种进制，最多使用的是十进制与十六进制，数字范围为0~15(0x0~0xf)
    console.SetColor(2, 0xe);
    cout << "there are different ways to \"SetColor\"" << endl;
    // 使用文字进行设置，这里的文字与命令行控制台的文字一致，前面一个字符是背景色，后一个字符是前景色(字体颜色)
    console.SetColor("4e");
    cout << "in order to make it easier to understand" << endl;
    // 使用自带的文本属性进行设置，这里的文本属性需要包含前景和背景色，需要使用逻辑或链接在一起
    console.SetColor(BACK_BLACK | FORE_WHITE);
    cout << "the way using text attribute is not recommended" << endl;
    // 当你使用鼠标拖动窗口大小时，会影响部分文本的颜色出现错误

    console.Pause("Press any key to continue...");
    return 0;
}