#include <iostream>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

int main() {
    Console console;
    console.SetCodePage(CODEPAGE_UTF_8);
    //在使用时记得将您的终端字体设置为Nerd Font
    //详见 https://www.nerdfonts.com/cheat-sheet
    console.SetColor(color::black, color::pink);
    cout << "\ue0b6"; 
    console.SetColor(color::pink, color::black);
    cout << "  ProAyeR@Laptop-proayer  ";
    console.SetColor(color::blue, color::pink);
    cout << "\ue0b0";
    console.SetColor(color::blue, color::olive);
    cout << "  perfect_console  ";
    console.SetColor(color::black, color::blue);
    cout << "\ue0b0";
    //如下文本能否正常显示?(没有小方块)能则说明您字体安装正确
    //  ProAyeR@Laptop-proayer    perfect_console  
    console.Pause();
    return 0;
}