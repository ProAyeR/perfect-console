#include <iostream>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

int main()
{
    Console console;
    console.Cur::SetVisible(1);
    console.Cur::SetSize(100); // 100是一整个方块，1是只有一条线
    while (1)
    {
        int code = console.GetEventCode(1);      //这里参数是1就会修改鼠标的位置到用户点击处
        Position curpos = console.Cur::GetPos(); //鼠标位置已经修改，直接用这个函数获取鼠标位置即可
        console.Cur::SetPos(0, 0);
        cout << console.SetColor(0, 7) << curpos.X << "," << curpos.Y << "    "; //显示一下鼠标的坐标，可以帮助你理解控制台中坐标的位置
        console.Cur::SetPos(curpos);
        if (code == MOUSE_LEFT_BUTTON_PRESSED)
            cout << console.SetColor(1, 0) << "@";
        else if (code == MOUSE_RIGHT_BUTTON_PRESSED)
            cout << console.SetColor(2, 0) << "#";
        else if (code == MOUSE_MIDDLE_BUTTON_PRESSED)
            cout << console.SetColor(4, 0) << "%";
        else if (code == MOUSE_ROLL_UP)
            console.MoveText(console.GetScreenSec(), {0, -1});
        else if (code == MOUSE_ROLL_DOWN)
            console.MoveText(console.GetScreenSec(), {0, 1});
        else
            cout << console.SetColor(0, 0) << " ";
        console.Cur::SetPos(curpos); //为了美观，再设置一次光标位置
    }
    return 0;
}