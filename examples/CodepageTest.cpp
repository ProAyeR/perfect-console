#include <iostream>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

int main()
{
    //为什么要转换代码页?
    //正常来说，由UTF-8代码编写的程序文件，其中的中文是无法在正常代码页中显示的
    //也就是说中文会显示乱码，但是如果使用了代码页转换之后，就能够正常显示中文的文字
    //示范如下
    Console console;
    cout << "代码文件编码:UTF-8" << endl;
    cout << "中文无法正常显示" << endl;
    string str;
    cin >> str;
    cout << "中文的正常输入之后输出却受到文本限制:" << endl //这里经过测试，"文本限制"这几个字会影响到输出是否正常
         << str << endl;
    console.Pause();
    console.SetCodePage(CODEPAGE_UTF_8);
    cout << "现在控制台代码页改为了UTF-8的代码页,也就是65001" << endl;
    cout << "中文可以正常显示" << endl;
    cin >> str;
    cout << "中文却不可以正常输入后输出了" << endl
         << str << endl;
    console.Pause();
    console.SetCodePage(CODEPAGE_ANSI);
    // cout << str << endl;
    cout << "一般在选择代码页时，需要考虑是否有输入与输出，如果需要支持UTF-8代码，需要使用wstring与WCHAR等数据类型支持宽字符" << endl;
    console.Pause();
    return 0;
}