#include <iostream>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

int main()
{
    Console console;
    console.SetCodePage(65001); //有些字体需要改变代码页才能使用
    cout << console.SetColor(GetConsoleColor("9f")) << "中文测试，你好世界!" << endl;
    cout << "FontSize:" << console.GetFontSize().Y << endl; //这里未使用点阵字体所以只需要显示Y

    console.SetFont("微软雅黑"); //控制台本身就不支持这个字体
    console.Pause("Now=微软雅黑\n");
    console.SetFont(L"黑体"); //这个字体被支持，这里涉及wchar_t问题，所以只能使用常宽字符串的方式传递
    console.Pause("Now=黑体\n");
    console.SetFont("Cascadia Code"); //这个字体被支持
    console.Pause("Now=Cascadia Code\n");

    //具体需要得知控制台是否支持某个字体，可以在控制台被设置为某个代码页时，查看控制台窗口的属性，在字体一栏中查看，有列出来的就是支持
    cout << console.SetColor(0, 7);
    console.CleanScreen();

    cout << "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-=!@#$%^&*()_+[]{}<>,.:\";'/?中文" << endl;
    console.SetBitmapFont(); // 默认为8*16
    console.Pause("Now=8*16\n");
    console.SetBitmapFont({8, 8}); //貌似是最小支持尺寸
    console.Pause("Now=8*8\n");
    console.SetBitmapFont({7, 7}); //这里就不会有反应
    console.Pause("Now=7*7\n");
    console.SetBitmapFont({16, 16});
    console.Pause("Now=16*16\n");
    console.SetBitmapFont({12, 7});
    console.Pause("Now=12*7\n");
    console.SetBitmapFont({24, 24});
    console.Pause("Now=24*24\n");
    console.SetBitmapFont({10, 26});
    console.Pause("Now=10*26\n");
    console.Pause("Press any key to exit....");

    return 0;
}