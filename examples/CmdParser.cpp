#include <iostream>
#include <conio.h>
#include <vector>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

//字符串分割函数，pattern会单独分在一个容器单元内
vector<string> text_split(string str, char pattern)
{
    vector<string> result;
    string tempstr;
    result.clear();
    for (int i = 0; i < str.length(); i++)
    {
        if (str.at(i) == pattern)
        {
            if (!tempstr.empty())
                result.push_back(tempstr);
            tempstr.clear();
            tempstr += pattern;
            result.push_back(tempstr);
            tempstr.clear();
        }
        else
            tempstr += str.at(i);
    }
    if (!tempstr.empty()) //把最后一个没写入的子串写入
        result.push_back(tempstr);
    return result;
}

//将含有括号的和引号的字符串合并在一起
vector<string> text_combine(vector<string> src, char pattern)
{
    int combine_flag = 0;
    vector<string> result;
    string tempstr;
    result.clear();
    for (int i = 0; i < src.size(); i++)
        if (!src[i].empty())
        {
            if (src[i].at(0) == '\"') //前符号处理
                combine_flag = 1;     //不同的匹配符号要使用不同的flag
            else if (src[i].at(0) == '(')
                combine_flag = 2;
            else if (!combine_flag) //没有检测到前符号
                result.push_back(src[i]);
            if (combine_flag) //后符号处理
            {
                tempstr += src[i];
                if ((combine_flag == 1 && src[i].at(src[i].length() - 1) == '\"') ||
                    (combine_flag == 2 && src[i].at(src[i].length() - 1) == ')')) //检测后符号
                {
                    result.push_back(tempstr);
                    tempstr.clear();
                    combine_flag = 0;
                }
            }
        }
    if (!tempstr.empty()) //把最后一个没写入的子串写入
        result.push_back(tempstr);
    return result;
}

bool find_any(string str, string pattern)
{
    for (int i = 0; i < str.length(); i++)
        if (str.substr(i, pattern.length()) == pattern)
            return true;
    return false;
}

void ShowCmd(Console &console, string str, Position pos)
{
    vector<string> raw = text_split(str, ' ');
    vector<string> cmd = text_combine(raw, ' ');
    console.Cur::SetPos(pos);
    for (int i = 0; i < cmd.size(); i++) //对每一个代码段进行上色
    {
        if (i == 0) //说明这是命令或空格
            cout << console.SetColor(color::black, color::purple) << cmd[0];
        else if (cmd[i].at(0) == '-' || cmd[i].at(0) == '/') //说明这是一个参数
            cout << console.SetColor(color::black, color::yellow) << cmd[i];
        else if (cmd[i].at(0) == '\"') //说明这是一个字符串
            cout << console.SetColor(color::black, color::cyan) << cmd[i];
        else if (cmd[i].at(0) == '(') //说明这是一个指令串
            cout << console.SetColor(color::black, color::red) << cmd[i];
        else if (find_any(cmd[i], ".")) //说明这是一个文件、路径
            cout << console.SetColor(color::black, color::brown) << cmd[i];
        else if (find_any(cmd[i], "-")) //说明这是一个文字参数
            cout << console.SetColor(color::black, color::gray) << cmd[i];
        else
            cout << console.SetColor(color::black, color::pale) << cmd[i];
    }
}

int main()
{
    Console console;
    console.SetCodePage(65001); //需要改变代码页才能使用
    console.SetFont("Cascadia Code", 22);

    Position cmdpos = {0, 0};
    Section screen = console.GetScreenSec();
    string cmd;
    string original_title_str = console.GetTitle();
    cmd.clear();
    //建议使用 command get-file -origin .\file.cpp "message text" command (command code) 来测试上色效果
    while (1)
    {
        char c = getch();
        if (c == '\r' || c == '\t' || c == '\n')
        {
            console.SetTitle(original_title_str + " - [" + cmd + "]"); //用title来验证命令读取是否正确
            cmd.clear();
            cmdpos.Y += 1;
        }
        if (c == 22)  // ctrl v
            continue; //我懒得再写一个粘贴逻辑了
        cmd += c;
        if (c == '\b' && cmd.length() > 0)         //要最后检测退格以防残留字符
            cmd = cmd.substr(0, cmd.length() - 2); //要多减去一个'\0'
        console.Clean({cmdpos.X, cmdpos.Y, screen.Right, cmdpos.Y});
        ShowCmd(console, cmd, cmdpos);
    }
    return 0;
}
