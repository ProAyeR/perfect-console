#include <iostream>
#include <windows.h>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

int main()
{
    Console console;
    console.SetCodePage(65001);
    cout << console.SetColor(GetConsoleColor("9f")) << "窗口设置为全屏" << endl;
    console.FullScreen(); //全屏将窗口移动到-8,0的位置
    // console.Maximize();   //最大化不会改变窗口的位置
    while (1)
    {
        Position pos = console.Where();
        cout << "窗口位置已改变，为:" << pos.X << "," << pos.Y << endl;
        console.Move(pos.X - 8, pos.Y); //模拟窗口抖动
        Sleep(50);
        console.Move(pos.X, pos.Y);
        Sleep(50);
        console.Move(pos.X - 8, pos.Y);
        Sleep(50);
        console.Move(pos.X, pos.Y);
        Sleep(50);
        console.Pause();
    }
    return 0;
}