#include <iostream>
#include "..\Console.cpp"
//版本v1.1

using namespace std;
using namespace perfect_console;

int main()
{
    Console console;                // 控制台类 console
    console.SetSize(80, 25);        // 设置窗口大小
    console.Cur::SetVisible(false); // 光标设置为隐身
    bool quit = false;
    Position pos = {0, 0};
    while (!quit)
    {
        string word = "";
        int sta = 0;
        int code = console.GetKeyEvent(&sta); // 允许使用ctrl+v粘贴文本
        console.Clean({28, 12, 80, 12}); // 清理出一片区域用于显示文本
        console.Clean({0, 13, 15, 13});
        // code返回的键值部分与字符值(ASCII)相等，当然，只有部分
        if ((code >= 'A' && code <= 'Z') || (code >= '0' && code <= '9') || code == ' ')
        {
            word = (char)code;
            console.Cur::SetPos(pos);
            cout << word; // 把这些允许的键值显示出来
            if (++pos.X >= 80)
            {
                pos.X = 0;
                if (++pos.Y >= 25)
                    pos.Y = 24; // 当80,25被写后，控制台会自动滚动一次，因为隐身的光标跑到下一行去了
            }
        }

        if (sta & SHIFT_PRESSED) // 这里要用逻辑与判断是否拥有该状态
            word = "shift+" + word;
        if (sta & LEFT_ALT_PRESSED)
            word = "left_alt+" + word;
        if (sta & RIGHT_ALT_PRESSED)
            word = "right_alt+" + word;
        if (sta & LEFT_CTRL_PRESSED)
            word = "left_ctrl+" + word;
        if (sta & RIGHT_CTRL_PRESSED)
            word = "right_ctrl+" + word;
        if (sta & CAPSLOCK_ON)
            word = "capslock+" + word;

        console.Cur::SetPos(32, 12);
        cout << code << ":" << word; // 显示键值
    }
    return 0;
}